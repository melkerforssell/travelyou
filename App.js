import React from 'react'
import { Platform, StatusBar, StyleSheet, View } from 'react-native'
import { AppLoading, Asset, Font, Icon } from 'expo'
import AppNavigator from './navigation/AppNavigator'
import * as firebase from 'firebase'
// At the top of your file

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = { loading: true }
  }

  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
    })
    this.setState({ loading: false })
  }
  render() {
    if (this.state.loading) {
      return <AppLoading />
    } else {
      return <AppNavigator />
    }
  }
}
