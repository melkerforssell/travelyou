import React from 'react'
import { Platform } from 'react-native'
import {
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation'

import TabBarIcon from '../components/TabBarIcon'
import HomeScreen from '../screens/Feed'
import ExploreScreen from '../screens/Explore'
import ProfileScreen from '../screens/Profile'

import TripScreen from '../screens/Trip'
import PostScreen from '../screens/Post'
import NewTripScreen from '../screens/NewTrip'

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Post: PostScreen,
  },
  { initialRouteName: 'Home' }
)

HomeStack.navigationOptions = {
  tabBarLabel: 'Feed',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-paper` : 'md-paper'}
    />
  ),
}

const ExploreStack = createStackNavigator({
  Explore: ExploreScreen,
  Post: PostScreen,
})

ExploreStack.navigationOptions = {
  tabBarLabel: 'Explore',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-map' : 'md-map'}
    />
  ),
}

const ProfileStack = createStackNavigator(
  {
    Profile: ProfileScreen,
    Trip: TripScreen,
    NewTrip: NewTripScreen,
    Post: PostScreen,
  },
  { initialRouteName: 'Profile' }
)

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
    />
  ),
}

export default createBottomTabNavigator({
  HomeStack,
  ExploreStack,
  ProfileStack,
})
