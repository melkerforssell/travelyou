import React from 'react'
import { Button, Image, View } from 'react-native'
import { ImagePicker, Permissions, Constants } from 'expo'

export default class ImagePickerExample extends React.Component {
  state = {
    image: null,
  }
  onChange() {
    this.props.passImage(this.state.image)
  }
  render() {
    let { image } = this.state

    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Button
          title="Pick an image from camera roll"
          onPress={this.useLibraryHandler}
        />
        {image && (
          <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />
        )}
      </View>
    )
  }

  askPermissionsAsync = async () => {
    await Permissions.askAsync(Permissions.CAMERA)
    await Permissions.askAsync(Permissions.CAMERA_ROLL)
    // you would probably do something to verify that permissions
    // are actually granted, but I'm skipping that for brevity
  }

  useLibraryHandler = async () => {
    await this.askPermissionsAsync()
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: false,
    })
    this.setState({ image: result.uri })
    this.onChange()
  }

  useCameraHandler = async () => {
    await this.askPermissionsAsync()
    let result = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: false,
    })
    this.setState({ image: result.uri })
    this.onChange()
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    })

    console.log('result from image picker:', result)

    if (!result.cancelled) {
      this.setState({ image: result.uri })
      this.onChange()
    }
  }
}
