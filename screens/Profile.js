import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  Card,
  CardItem,
  Icon,
  H2,
} from 'native-base'
import { Col, Row, Grid } from 'react-native-easy-grid'
import { View } from 'react-native'

import { db } from '../db'
import { tripsRef, currentUser } from '../constants/constants'

const ONE_DAY = 86400 * 1000

export default class Profile extends Component {
  state = {
    trips: [],
  }

  static navigationOptions = {
    title: 'Profile',
  }

  componentDidMount() {
    tripsRef
      .orderByChild('user/name')
      .equalTo(currentUser.name)
      .on('child_added', snapshot => {
        const trip = snapshot.val()
        console.log('NEW TRIP!!!!!!!!!', trip)
        if (trip.posts) {
          const post_dates = Object.values(trip.posts).map(
            post => new Date(post.timestamp)
          )
          post_dates.sort((a, b) => a - b)
          this.state.trips.push({
            ...trip,
            key: snapshot.key,
            start: post_dates[0],
            end: post_dates[post_dates.length - 1],
          })
          this.setState({ trips: this.state.trips })
        }
      })
  }

  goToTrip = trip => {
    this.props.navigation.push('Trip', { ...trip })
  }

  render() {
    return (
      <Container>
        <Content padder>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail source={{ uri: currentUser.profilepic }} />
                <Body>
                  <Text>{currentUser.name}</Text>
                  {currentUser.location ? (
                    <Button small rounded iconLeft light>
                      <Icon name="pin" />
                      <Text>{currentUser.location}</Text>
                    </Button>
                  ) : null}
                  {currentUser.bio ? <Text note>{currentUser.bio}</Text> : null}
                </Body>
              </Left>
            </CardItem>
          </Card>

          <List>
            <ListItem itemHeader first>
              <H2>Your trips</H2>
            </ListItem>
            {this.state.trips.map(trip => (
              <ListItem key={trip.key}>
                <Body>
                  <Text>{trip.name}</Text>
                  <Text note>
                    {trip.start
                      ? `${trip.start.toLocaleDateString()} - ${
                          Date.now() - trip.end.getTime() < ONE_DAY
                            ? 'Now'
                            : trip.end.toLocaleDateString()
                        }`
                      : 'New'}
                  </Text>
                </Body>

                <Right>
                  <Button transparent onPress={() => this.goToTrip(trip)}>
                    <Text>View</Text>
                  </Button>
                </Right>
              </ListItem>
            ))}
          </List>
          <View style={{ padding: 15 }}>
            <Button
              title="Add trip"
              onPress={() => this.props.navigation.push('NewTrip')}
            >
              <Text>Add new trip</Text>
            </Button>
          </View>

          <List>
            <ListItem itemHeader first>
              <H2>Your friends</H2>
            </ListItem>
            {currentUser.friends.map((friend, idx) => (
              <ListItem
                key={idx}
                button
                onPress={() => console.log('Navigate to friend page')}
              >
                <Body>
                  <Text>{friend}</Text>
                </Body>

                <Right>
                  <Icon name="arrow-forward" />
                </Right>
              </ListItem>
            ))}
          </List>
        </Content>
      </Container>
    )
  }
}
