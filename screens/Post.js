import React, { Component } from 'react'
import { Container, Content } from 'native-base'

import SinglePost from '../components/SinglePost'

export default class Post extends Component {
  render() {
    // console.log("params")
    // console.log(this.props.navigation.state.params)
    // const { user, tripName, date, content, caption, likes } = this.props.navigation.state.params

    return (
      <Container>
        <Content>
          <SinglePost {...this.props.navigation.state.params} />
        </Content>
      </Container>
    )
  }
}
