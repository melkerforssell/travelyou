import React, { Component } from 'react'
import { Image, Modal, View, StyleSheet, Alert } from 'react-native'
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Textarea,
  Form,
  Right,
  Title,
  Input,
  Item,
} from 'native-base'
import Picker from 'react-native-picker-select'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'

import uuid from 'uuid'
import { isEmpty } from 'lodash'

import {
  tripsRef,
  currentUser,
  GOOGLE_MAPS_API_KEY,
} from '../constants/constants'
import ImagePicker from '../imagePicker'
import { db, fb } from '../db'

async function uploadImageAsync(uri) {
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.onload = function() {
      resolve(xhr.response)
    }
    xhr.onerror = function(e) {
      console.log(e)
      reject(new TypeError('Network request failed'))
    }
    xhr.responseType = 'blob'
    xhr.open('GET', uri, true)
    xhr.send(null)
  })
  const ref = fb
    .storage()
    .ref()
    .child(uuid.v4())

  const snapshot = await ref.put(blob)

  blob.close()

  return await snapshot.ref.getDownloadURL()
}

const addPost = async state => {
  var trip = tripsRef.child(state.tripValue).child('posts')

  uploadUrl = state.image ? await uploadImageAsync(state.image) : null
  trip.push({
    caption: state.caption,
    comments: [
      {
        text: '',
        user: '',
      },
    ],
    content: uploadUrl,
    timestamp: new Date().getTime(),
    likes: 0,
    place: state.place,
  })
}

// addTrip = (state) => {
//   testRef.push({
//   name: state.tripName,
//   places: [ {
//     "latitude" : 22.142706,
//     "longitude" : 114.101998,
//     "name" : state.place
//   } ],
//   posts : [ {
//     caption : state.caption,
//     comments : [ {
//       text : "",
//       user : ""
//     } ],
//     content : "https://5.imimg.com/data5/EF/RQ/MY-3030942/lenovo-desktop-computer-500x500.jpg",
//     date : date,
//     time : time,
//     likes : 0,
//     place : state.place,
//   } ],

//   user : {
//     name : "testUser",
//     profilepic : "https://img.pngio.com/robot-png-free-images-toppng-transparent-image-download-robot-png-480_480.png"
//   }
// })};

export default class NewPost extends Component {
  state = {
    tripValue: '',
    image: null,
    caption: '',
    userTripOptions: [],
    currentLocation: {},
    place: {},
    submitDisabled: false,
  }

  static defaultProps = {
    onCancel: () => {},
    onSubmit: () => {},
  }

  componentDidMount() {
    tripsRef
      .orderByChild('user/name')
      .equalTo(currentUser.name)
      .once('value', snapshot => {
        const userTripOptions = []

        snapshot.forEach(tripSnapshot => {
          const tripKey = tripSnapshot.key
          const tripName = tripSnapshot.child('name').val()

          userTripOptions.push({ label: tripName, value: tripKey })
        })

        this.setState({ userTripOptions })
      })

    navigator.geolocation.getCurrentPosition(
      location => {
        currentLocation = {
          lat: location['coords']['latitude'],
          long: location['coords']['longitude'],
        }
        this.setState({ currentLocation })
      },
      error => {
        console.warn(error)
      }
    )
  }

  handleChangeTrip = (value, index) => {
    // ooor this --------------------
    this.setState({ tripValue: value })
  }

  handleSubmitPost = async () => {
    if (!this.state.tripValue) {
      Alert.alert('Please select the trip')
      return false
    }
    if (isEmpty(this.state.place)) {
      Alert.alert('Please select the place')
      return false
    }
    this.setState({ submitDisabled: true })
    await addPost(this.state)
    this.setState({ submitDisabled: false })
    this.props.onSubmit()
  }

  handleChangePlace = (
    { description, place_id, structured_formatting: { main_text } },
    { geometry }
  ) => {
    this.setState({
      place: {
        description: main_text,
        place_id,
        coordinates: {
          latitude: geometry.location.lat,
          longitude: geometry.location.lng,
        },
      },
    })
  }

  handleChangeDesc = e => {
    this.setState({ caption: e.nativeEvent.text })
  }

  onSelectImage = data => {
    this.setState({ image: data })
  }

  // handleSubmitTrip = () => {
  //   console.log("adding item:",this.state);
  //   addTrip(this.state)
  //   this.closeModal()
  //   this.clearState()

  // }

  clearState = () => {
    //otherwise caption, place, tripname and so on is still there
    this.setState({
      caption: '',
      place: '',
    })
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button hasText transparent onPress={this.props.onCancel}>
              <Text>Cancel</Text>
            </Button>
          </Left>
          <Body>
            <Title>Create post</Title>
          </Body>
          <Right>
            <Button
              hasText
              transparent
              disabled={this.state.submitDisabled}
              onPress={this.handleSubmitPost}
            >
              <Text>Save</Text>
            </Button>
          </Right>
        </Header>
        <Content padder>
          <Form>
            <Picker
              placeholder={{
                label: 'Select a trip...',
                value: null,
              }}
              items={this.state.userTripOptions}
              onValueChange={this.handleChangeTrip}
              value={this.state.tripValue}
              style={pickerSelectStyles}
            />
            {/* <Input placeholder='Place'   onChange={this.handleChangePlace} /> */}
            <GooglePlacesAutocomplete
              placeholder="Select a place..."
              minLength={2}
              query={{
                key: GOOGLE_MAPS_API_KEY,
                language: 'en',
                location: `${this.state.currentLocation.lat},${
                  this.state.currentLocation.long
                }`,
                radius: 200,
                // strictbounds: true
              }}
              nearbyPlacesAPI="GooglePlacesSearch"
              currentLocation={false}
              debounce={200}
              styles={{
                textInputContainer: {
                  backgroundColor: 'rgba(0,0,0,0)',
                  borderColor: 'rgba(0,0,0,0)',
                  borderTopWidth: 0,
                  borderBottomWidth: 0,
                  marginTop: 15,
                },
                textInput: {
                  marginLeft: 0,
                  marginRight: 0,
                  marginTop: 0,
                  marginBottom: 0,
                  height: 'auto',
                  fontSize: 16,
                  borderWidth: 1,
                  borderColor: 'gray',
                  borderRadius: 4,
                  paddingVertical: 12,
                  paddingHorizontal: 10,
                },
              }}
              listViewDisplayed={false}
              onPress={this.handleChangePlace}
              fetchDetails={true}
              GooglePlacesDetailsQuery={{
                // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                fields: 'geometry',
              }}
            />

            <Textarea
              rowSpan={5}
              bordered
              placeholder="Insert your description of the post"
              onChange={this.handleChangeDesc}
              style={{ padding: 15, marginTop: 15 }}
            />

            <ImagePicker passImage={this.onSelectImage} />
          </Form>
          {/* <Button success onPress={() => this.handleSubmitTrip()}><Text> Add trip </Text></Button> */}

          {/* <View
            style={{ flexDirection: 'row', justifyContent: 'space-around' }}
          > */}
          {/* <Button success onPress={this.handleSubmitPost}>
              <Text> Add Post </Text>
            </Button> */}
          {/* <Button danger onPress={this.props.onCancel}>
              <Text> Close </Text>
            </Button> */}
          {/* </View> */}
        </Content>
      </Container>
    )
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'gray',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
})
