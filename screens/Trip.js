import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Animated,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native'
import { Container, Text } from 'native-base'

import MapView from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions'

import { currentUser, GOOGLE_MAPS_API_KEY } from '../constants/constants'

//scroll animation ref: https://codedaily.io/tutorials/9/Build-a-Map-with-Custom-Animated-Markers-and-Region-Focus-when-Content-is-Scrolled-in-React-Native

function createColor() {
  var letters = '0123456789ABCDEF'
  var color = '#'
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)]
  }
  return color
}

const { width, height } = Dimensions.get('window')

const CARD_HEIGHT = height / 4
const CARD_WIDTH = CARD_HEIGHT - 50

export default class Trip extends Component {
  constructor(props) {
    super(props)
    this.handleMarkPress = this.handleMarkPress.bind(this)
    this.handleMapPress = this.handleMapPress.bind(this)
  }
  state = {
    tripValue: '',
    userTripOptions: [],

    tripName: '',
    showCards: false,
    markers: [],
    region: {
      latitude: 22.28552,
      longitude: 114.15769,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    },

    coordinates: [],
    color: createColor(),
  }

  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('name', ''),
  })

  handleMarkPress(selectedTrip) {
    this.setState(oldState => ({
      showCards: !oldState.showCards,
      tripName: selectedTrip,
    }))
  }

  handleMapPress() {
    this.setState({ showCards: false, tripName: '' })
  }

  componentWillMount() {
    this.index = 0
    this.animation = new Animated.Value(0)
  }
  componentDidMount() {
    // We should detect when scrolling has stopped then animate
    // We should just debounce the event listener here

    const markers = []
    const coordinates = []
    const posts = this.props.navigation.getParam('posts')
    const name = this.props.navigation.getParam('name')

    Object.values(posts).forEach(post => {
      if (post.place.coordinates !== undefined) {
        //TO DO: could probably condense this string manipulation
        coordinateStr = {}
        coordinateStr = JSON.stringify(post.place.coordinates)
        coordinateStr = coordinateStr.replace(/\"/g, '')
        coordinateStr = coordinateStr.replace(/{/g, '')
        coordinateStr = coordinateStr.replace(/}/g, '')

        tempLatitude = coordinateStr.split(':', 3)[1]
        tempLatitude = tempLatitude.split(',')[0]
        tempLongitude = coordinateStr.split(':', 3)[2]

        coordinateObj = {
          latitude: parseFloat(tempLatitude),
          longitude: parseFloat(tempLongitude),
        }
        coordinates.push(coordinateObj)

        markers.push({
          coordinate: coordinateObj,
          trip: name,
          description: post.caption,
          image: post.content,
          user: currentUser,
          timestamp: post.timestamp,
          likes: post.likes,
        })
      }

      this.handleMarkPress(null) //needed to make the markers appear initially (for some reasom)
    })

    this.setState({ markers, coordinates })

    this.animation.addListener(({ value }) => {
      let index = Math.floor(value / CARD_WIDTH + 0.3) // animate 30% away from landing on the next item
      if (index >= this.state.markers.length) {
        index = this.state.markers.length - 1
      }
      if (index <= 0) {
        index = 0
      }

      clearTimeout(this.regionTimeout)
      this.regionTimeout = setTimeout(() => {
        if (this.index !== index) {
          this.index = index

          if (
            name == this.state.tripName &&
            this.state.coordinates !== undefined
          ) {
            if (index >= this.state.coordinates.length) {
              index = this.state.coordinates.length - 1
            }
            const aniCoordinate = this.state.coordinates[index]
            console.log(aniCoordinate)

            this.map.animateToRegion(
              {
                ...aniCoordinate,
                latitudeDelta: this.state.region.latitudeDelta,
                longitudeDelta: this.state.region.longitudeDelta,
              },
              350
            )
          }
        }
      }, 10)
    })
  }

  render() {
    const posts = this.props.navigation.getParam('posts')

    const interpolations = this.state.markers.map((marker, index) => {
      const inputRange = [
        (index - 1) * CARD_WIDTH,
        index * CARD_WIDTH,
        (index + 1) * CARD_WIDTH,
      ]
      const scale = this.animation.interpolate({
        inputRange,
        outputRange: [1, 2.5, 1],
        extrapolate: 'clamp',
      })
      const opacity = this.animation.interpolate({
        inputRange,
        outputRange: [0.35, 1, 0.35],
        extrapolate: 'clamp',
      })
      return { scale, opacity }
    })

    return (
      <Container>
        <MapView
          ref={map => (this.map = map)}
          initialRegion={this.state.region}
          style={styles.container}
          onPress={this.handleMapPress}
        >
          {this.state.markers.map((marker, index) => {
            const scaleStyle = {
              transform: [
                {
                  scale: interpolations[index].scale,
                },
              ],
            }
            const opacityStyle = {
              opacity: interpolations[index].opacity,
            }
            return (
              <MapView.Marker
                key={index}
                coordinate={marker.coordinate}
                onSelect={() => this.handleMarkPress(marker.trip)}
                onPress={() => this.handleMarkPress(marker.trip)}
              >
                <Animated.View style={[styles.markerWrap, opacityStyle]}>
                  <Animated.View style={[styles.ring, scaleStyle]} />
                  <View style={styles.marker} />
                </Animated.View>
              </MapView.Marker>
            )
          })}

          {Object.values(posts).map((post, postIdx) => {
            if (post.place && postIdx < Object.keys(posts).length - 1) {
              const originPlace = post.place
              const destPlace = posts[Object.keys(posts)[postIdx + 1]].place
              return (
                <MapViewDirections
                  key={postIdx}
                  apikey={GOOGLE_MAPS_API_KEY}
                  origin={`place_id:${originPlace.place_id}`}
                  destination={`place_id:${destPlace.place_id}`}
                  strokeWidth={4}
                  strokeColor={this.state.color}
                  mode="walking"
                />
              )
            }
          })}
        </MapView>
        <Animated.ScrollView
          horizontal
          scrollEventThrottle={1}
          showsHorizontalScrollIndicator={false}
          snapToInterval={CARD_WIDTH}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: this.animation,
                  },
                },
              },
            ],
            { useNativeDriver: true }
          )}
          style={styles.scrollView}
          contentContainerStyle={styles.endPadding}
        >
          {this.state.markers.map((marker, index) => (
            <View
              key={index}
              style={
                this.state.showCards && marker.trip == this.state.tripName
                  ? styles.card
                  : { display: 'none' }
              }
            >
              <TouchableOpacity
                style={
                  this.state.showCards && marker.trip == this.state.tripName
                    ? styles.overCard
                    : { display: 'none' }
                }
                onPress={() => {
                  this.props.navigation.push('Post', {
                    user: marker.user,
                    tripName: marker.trip,
                    timestamp: marker.timestamp,
                    content: marker.image,
                    caption: marker.description,
                    likes: marker.likes,
                  })
                }}
              >
                <Image
                  source={{ uri: marker.image }}
                  style={styles.cardImage}
                  resizeMode="cover"
                />
                <View style={styles.textContent}>
                  <Text numberOfLines={1} style={styles.cardtitle}>
                    {marker.title}
                  </Text>
                  <Text numberOfLines={1} style={styles.cardDescription}>
                    {marker.description}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          ))}
        </Animated.ScrollView>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    position: 'absolute',
    bottom: 30,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    padding: 10,
    elevation: 2,
    backgroundColor: '#FFF',
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: 'hidden',
  },
  overCard: {
    ...StyleSheet.absoluteFillObject,
  },
  cardImage: {
    flex: 3,
    width: '100%',
    height: '100%',
    alignSelf: 'center',
  },
  textContent: {
    flex: 1,
  },
  cardtitle: {
    fontSize: 12,
    marginTop: 5,
    fontWeight: 'bold',
  },
  cardDescription: {
    fontSize: 12,
    color: '#444',
  },
  markerWrap: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: 'rgba(130,4,150, 0.9)',
  },
  ring: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: 'rgba(130,4,150, 0.3)',
    position: 'absolute',
    borderWidth: 1,
    borderColor: 'rgba(130,4,150, 0.5)',
  },
})
