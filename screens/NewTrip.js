import React, { Component } from 'react'
import { View, StyleSheet, TextInput, Alert } from 'react-native'
import CountryPicker, {
  getAllCountries,
} from 'react-native-country-picker-modal'
import { SegmentedControls } from 'react-native-radio-buttons'
import { TextField } from 'react-native-material-textfield'
import {
  Container,
  Content,
  Text,
  Button,
  Form,
  Item,
  Label,
  Input,
  H3,
  Header,
  Body,
  Left,
  Right,
  Title,
} from 'native-base'

import { db } from '../db'
import { currentUser } from '../constants/constants'

export default class NewTrip extends Component {
  constructor(props) {
    super(props)
    this.state = {
      nameInput: '',
      cca2: 'HK',
      selectedTripOption: 'Solo Trip',
      selectedBudgetOption: 'Free',
      selectedCategory: 'Leisure',
      descriptionInput: '',
    }
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Create new trip',
    headerRight: (
      <Button hasText transparent onPress={navigation.getParam('addTrip')}>
        <Text>Create</Text>
      </Button>
    ),
  })

  componentDidMount() {
    this.props.navigation.setParams({
      addTrip: () => {
        this.addTrip(this.state.nameInput, this.state.descriptionInput)
      },
    })
  }

  addTrip = (tripName, tripDescription) => {
    if (!tripName) {
      Alert.alert('Please enter the trip name')
      return false
    }
    db.ref('/trip').push({
      description: tripDescription,
      name: tripName,
      // posts: {
      //   0: {
      //     caption: 'My newly added trip',
      //     comments: { text: '', user: '' },
      //     content: '',
      //     date: '2015-01-08',
      //     likes: 0,
      //     place: '',
      //   },
      // },
      user: {
        name: currentUser.name,
        profilepic: currentUser.profilepic,
      },
    })
    this.createAlert()
  }

  createAlert = () => {
    Alert.alert(
      'Trip added successfully',
      '',
      [
        {
          text: 'OK',
          onPress: () => {
            this.props.navigation.navigate('Profile')
          },
        },
      ],
      { cancelable: false }
    )
  }

  render() {
    const categories = ['Leisure', 'Sightseeing', 'Hiking', 'Shopping']
    const tripOptions = ['Solo Trip', 'Group Trip']
    const budgetOptions = ['Free', 'Budget', 'Costly']

    function setSelectedCategory(selectedOption) {
      this.setState({
        selectedCategory: selectedOption,
      })
    }

    function setSelectedTripOption(selectedOption) {
      this.setState({
        selectedTripOption: selectedOption,
      })
    }

    function setSelectedBudgetOption(selectedOption) {
      this.setState({
        selectedBudgetOption: selectedOption,
      })
    }

    return (
      <Container>
        <Content padder>
          <Form />

          <Item>
            <Label>Trip name</Label>
            <Input
              onChangeText={value => this.setState({ nameInput: value })}
              value={this.state.nameInput}
            />
          </Item>
          <Item>
            <Label>Trip description</Label>
            <Input
              onChangeText={value => this.setState({ descriptionInput: value })}
              value={this.state.descriptionInput}
              // multiline={true}
            />
          </Item>
          <View style={styles.countryContainer}>
            <Text style={styles.nameText}>Country: {this.state.cca2}</Text>
            <View style={styles.flag}>
              <CountryPicker
                onChange={value => this.setState({ cca2: value.cca2 })}
                cca2={this.state.cca2}
                translation="eng"
                label={this.state.cca2}
              />
            </View>
          </View>

          <H3 style={{ paddingTop: 5, paddingBottom: 5 }}>Trip Details</H3>

          <View>
            <SegmentedControls
              direction={'column'}
              options={categories}
              onSelection={setSelectedCategory.bind(this)}
              selectedOption={this.state.selectedCategory}
            />
          </View>
          <View style={styles.segmentedControls}>
            <SegmentedControls
              options={tripOptions}
              onSelection={setSelectedTripOption.bind(this)}
              selectedOption={this.state.selectedTripOption}
            />
          </View>
          <View style={styles.segmentedControlsBudget}>
            <SegmentedControls
              options={budgetOptions}
              onSelection={setSelectedBudgetOption.bind(this)}
              selectedOption={this.state.selectedBudgetOption}
            />
          </View>
          {/* <View style={styles.button}>
            <Button
              title="Create"
              onPress={() =>
                this.addTrip(this.state.nameInput, this.state.descriptionInput)
              }
            >
              <Text>Create</Text>
            </Button>
          </View> */}
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 50,
    marginLeft: 5,
    marginRight: 5,
  },
  title: {
    fontWeight: '600',
    color: '#2089dc',
    marginTop: 5,
    fontSize: 20,
  },
  nameText: {
    marginTop: 10,
    fontSize: 25,
    textAlign: 'left',
  },
  countryContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 15,
    marginBottom: 15,
  },
  flag: {
    marginTop: 13,
    marginLeft: 10,
  },
  segmentedControls: {
    marginTop: 10,
  },
  segmentedControlsBudget: {
    marginTop: 10,
    marginBottom: 50,
  },
  button: {
    marginTop: 10,
  },
})
