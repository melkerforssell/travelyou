import React, { Component } from 'react'
import { Modal, View, StyleSheet, TouchableWithoutFeedback } from 'react-native'
import { Container, Content, Icon, Fab } from 'native-base'
import { orderBy, flatten, compact } from 'lodash'

import { db, fb } from '../db'
import NewPost from './NewPost'
import SinglePost from '../components/SinglePost'

import { tripsRef } from '../constants/constants'

var testRef = db.ref('/test/')
var imageRef = db.ref('/images/')

export default class HomeScreen extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    items: [],
    modalVisible: false,
  }

  static navigationOptions = {
    title: 'Feed',
  }

  goToPost = params => {
    this.props.navigation.push('Post', { ...params })
  }

  componentDidMount() {
    tripsRef.on('value', snapshot => {
      let data = snapshot.val()
      let items = data ? Object.values(data) : []

      this.setState({ items })
    })

    // TODO make it able to refresh whenever new stuff gets added

    // tripsRef.on('child_added', snapshot => {
    //   const data = snapshot.val()
    //   this.state.items.push(Object.values(data))
    //   this.setState({items: this.state.items})
    // })
  }

  getPosts() {
    const allPostProps = compact(
      flatten(
        this.state.items.map(trip => {
          if (!trip.posts) {
            return null
          }
          return Object.keys(trip.posts).map(key => ({
            tripKey: key,
            user: {
              profilepic: trip.user.profilepic,
              name: trip.user.name,
            },
            tripName: trip.name,
            timestamp: trip.posts[key].timestamp,
            content: trip.posts[key].content,
            caption: trip.posts[key].caption,
            likes: trip.posts[key].likes,
            place: trip.posts[key].place,
          }))
        })
      )
    )

    const sortedPostProps = orderBy(
      allPostProps,
      post => new Date(post.timestamp),
      ['desc']
    )

    // console.log('sorted', sortedPostProps)

    return sortedPostProps.map((post, idx) => (
      <TouchableWithoutFeedback
        key={`${post.tripKey}-${idx}`}
        onPress={() => this.goToPost(post)}
      >
        <View>
          <SinglePost {...post} />
        </View>
      </TouchableWithoutFeedback>
    ))

    // this.state.items.forEach((trip, tripIdx) => {
    //   Object.keys(trip.posts).forEach(key => {
    //     const postProps = {
    //       user: {
    //         profilepic: trip.user.profilepic,
    //         name: trip.user.name,
    //       },
    //       tripName: trip.name,
    //       date: trip.posts[key].date,
    //       content: trip.posts[key].content,
    //       caption: trip.posts[key].caption,
    //       likes: trip.posts[key].likes,
    //       place: trip.posts[key].place,
    //     }

    //     posts.push(
    //       <TouchableWithoutFeedback
    //         key={`${tripIdx}-${key}`}
    //         onPress={() => this.goToPost(postProps)}
    //       >
    //         <View>
    //           <SinglePost {...postProps} />
    //         </View>
    //       </TouchableWithoutFeedback>
    //     )
    //   })

    //   console.log('posts', posts)
    // })

    // return posts
  }
  openModal = () => {
    this.setState({ modalVisible: true })
  }

  closeModal = () => {
    this.setState({ modalVisible: false })
  }

  render() {
    /* to understand the db
      console.log("-----------")
     this.state.items.map(trip => {
      console.log("trip is:",trip)
      console.log("trip.name",trip.name)
      for (key in trip.posts){
        console.log("post in posts",key)
        console.log("e.posts[key].caption",trip.posts[key].caption)
      }
     })
     console.log("-----------")
     */

    return (
      <Container>
        <Content padder>{this.getPosts()}</Content>
        <Fab onPress={() => this.openModal()}>
          <Icon name="add" />
        </Fab>
        <Modal
          visible={this.state.modalVisible}
          animationType={'slide'}
          onRequestClose={() => this.closeModal()}
          closeModal={this.closeModal}
        >
          <NewPost onSubmit={this.closeModal} onCancel={this.closeModal} />
        </Modal>
      </Container>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'grey',
  },
  innerContainer: {
    alignItems: 'center',
  },
})
