import * as firebase from 'firebase'
const firebaseConfig = {
  apiKey: 'AIzaSyCKeqelwIxxoODSvJfWJ9FkJdakWC6Q2qM',
  authDomain: 'comp3330-mobiledev.firebaseapp.com',
  databaseURL: 'https://comp3330-mobiledev.firebaseio.com',
  projectId: 'comp3330-mobiledev',
  storageBucket: 'comp3330-mobiledev.appspot.com',
  messagingSenderId: '380814176393',
}

const fb = firebase.initializeApp(firebaseConfig)
const db = fb.database()
export { db, fb }
