import React, { Component } from 'react'

import { Image } from 'react-native'
import {
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
} from 'native-base'

export default class SinglePost extends Component {
  render() {
    const {
      user = {},
      tripName,
      timestamp,
      content,
      caption,
      likes,
      place = {},
    } = this.props

    return (
      <Card>
        <CardItem header bordered>
          <Left>
            <Thumbnail source={{ uri: user.profilepic }} />
            <Body>
              <Text>{user.name}</Text>
              <Text style={{ fontStyle: 'italic' }}>{tripName}</Text>
              {place.description ? (
                <Button small rounded iconLeft light>
                  <Icon name="pin" />
                  <Text>{place.description}</Text>
                </Button>
              ) : null}
              <Text note>published {new Date(timestamp).toLocaleString()}</Text>
            </Body>
          </Left>
        </CardItem>
        {!!content ? (
          <CardItem cardBody>
            <Image
              source={{ uri: content }}
              style={{ height: 300, width: 300, flex: 1 }}
            />
          </CardItem>
        ) : null}
        <CardItem>
          <Text>{caption}</Text>
        </CardItem>
        <CardItem footer bordered>
          <Left>
            <Button transparent textStyle={{ color: '#87838B' }}>
              <Text>{likes} likes </Text>
            </Button>
          </Left>
        </CardItem>
      </Card>
    )
  }
}
