# Installation

1. Download node if it isn’t already installed on your computer.
2. Navigate to the root of the unzipped folder.
3. Run `npm install -g expo-cli` in terminal.
4. Run `npm install` in terminal to install all of the necessary dependencies.
5. Run `expo start`. This should start a development server serving our application.
6. Press "i" into the terminal for iOS simulator (Must have XCode installed on the computer) or "a" for Android simulator
7. For iOS, if you don’t have the simulator, you will be prompted to download additional components from Xcode, which will include the simulator.
8. If everything is successful, a simulator will pop up and display the application.

If you would rather run the application on your smartphone, download the Expo app from the App Store or Play Store and scan the QR code that appears in the terminal & development server webpage to go to the app.


Expo iOS app: https://search.itunes.apple.com/WebObjects/MZContentLink.woa/wa/link?path=apps%2fexponent 

Expo Android app: https://play.google.com/store/apps/details?id=host.exp.exponent

UI Reference: https://docs.nativebase.io/

General Reference: https://expo.io/
