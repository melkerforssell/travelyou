import { db } from '../db'

export const tripsRef = db.ref('/trip/')
export const currentUser = {
  name: 'Melker Forssell',
  profilepic:
    'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png',
  bio: 'Travelling the world',
  location: 'Hong Kong',
  friends: ['Nola Chen', 'Walt Trustam', 'Jev Chomutovskij'],
}
export const GOOGLE_MAPS_API_KEY = 'AIzaSyCP1lhiIuYScFNY0yuryRhB2-8LK9Hdx-8'
